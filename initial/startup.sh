#!/bin/bash

read -p "gitlab username or email: "  username
read -s -p "password: "  password

# install curl
command -v curl || sudo apt-get install -y curl

# install docker
command -v docker || curl -sSL https://get.docker.com/ | sh

# install docker-compose
command -v docker-compose || sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
command -v docker-compose || ssudo chmod +x /usr/local/bin/docker-compose

# docker login
sudo docker login registry.gitlab.com -u $username -p $p

wget -q -O docker-compose.yml https://gitlab.com/gwth/public/raw/master/initial/docker-compose.yml

# pull image
sudo docker-compose pull || sudo docker logout registry.gitlab.com

# create and start
sudo docker-compose up -d || sudo docker logout registry.gitlab.com

# docker logout
sudo docker logout registry.gitlab.com